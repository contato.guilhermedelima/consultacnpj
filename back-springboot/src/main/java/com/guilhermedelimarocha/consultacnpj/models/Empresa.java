package com.guilhermedelimarocha.consultacnpj.models;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "EMPRESA")
public class Empresa {

    public Empresa(Long id, String cnpj, String cidade, String razaoSocial, String nomeFantasia, String situacao, String endereco, String telefone, LocalDateTime dhRegistro) {
        this.id = id;
        this.cnpj = cnpj;
        this.cidade = cidade;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.situacao = situacao;
        this.endereco = endereco;
        this.telefone = telefone;
        this.dhRegistro = dhRegistro;
    }

    public Empresa() {
    }

    public Empresa(String cnpj, String cidade, String razaoSocial, String nomeFantasia, String situacao) {
        this.cnpj = cnpj;
        this.cidade = cidade;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.situacao = situacao;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CNPJ", length = 15)
    private String cnpj;

    @Column(name = "CIDADE", length = 100)
    private String cidade;

    @Column(name = "RAZAO_SOCIAL", length = 100)
    private String razaoSocial;

    @Column(name = "NOME_FANTASIA", length = 100)
    private String nomeFantasia;

    @Column(name = "SITUACAO", length = 35)
    private String situacao;

    @Column(name = "ENDERECO", length = 255)
    private String endereco;

    @Column(name = "TELEFONE")
    private String telefone;

    @Column(name = "DH_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime dhRegistro;
}


