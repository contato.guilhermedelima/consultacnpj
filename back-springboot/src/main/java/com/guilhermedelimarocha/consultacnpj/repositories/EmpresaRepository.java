package com.guilhermedelimarocha.consultacnpj.repositories;

import com.guilhermedelimarocha.consultacnpj.models.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long> {


    public Empresa findByCnpj(String cnpj);

}
