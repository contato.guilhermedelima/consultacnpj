package com.guilhermedelimarocha.consultacnpj.controllers;

import com.guilhermedelimarocha.consultacnpj.models.Empresa;
import com.guilhermedelimarocha.consultacnpj.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/v1")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @GetMapping("/findByCnpj/{cnpj}")
    public ResponseEntity<?> findByCnpj(@PathVariable String cnpj) {
        Empresa empresa = empresaService.findByCnpj(cnpj);

        if (empresa == null) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(empresa);
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Empresa empresa) {
        try {
            return empresaService.save(empresa);
        } catch (Throwable ignored) {
            return ResponseEntity.internalServerError().build();
        }
    }
}
