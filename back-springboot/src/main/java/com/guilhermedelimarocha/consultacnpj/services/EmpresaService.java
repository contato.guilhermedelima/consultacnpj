package com.guilhermedelimarocha.consultacnpj.services;


import com.guilhermedelimarocha.consultacnpj.models.Empresa;
import org.springframework.http.ResponseEntity;

public interface EmpresaService {
    Empresa findByCnpj(String cnpj);

    ResponseEntity<?> save(Empresa empresa);
}
