package com.guilhermedelimarocha.consultacnpj.services.impl;

import com.guilhermedelimarocha.consultacnpj.models.Empresa;
import com.guilhermedelimarocha.consultacnpj.repositories.EmpresaRepository;
import com.guilhermedelimarocha.consultacnpj.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Map;

@Service
public class EmpresaServiceImpl implements EmpresaService {

    private final EmpresaRepository empresaRepository;
    private RestTemplate restTemplate;

    @Autowired
    public EmpresaServiceImpl(EmpresaRepository empresaRepository, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.empresaRepository = empresaRepository;
    }

    public Empresa findByCnpj(String cnpj) {
        String cnpjAjustado = cnpj.replaceAll("\\W+", "");
        Empresa empresa = empresaRepository.findByCnpj(cnpjAjustado);

        if (empresa == null) {
            try {
                ResponseEntity<?> objByCnpj = restTemplate.getForEntity("https://publica.cnpj.ws/cnpj/" + cnpjAjustado, Map.class);

                if (objByCnpj.getBody() instanceof Map) {
                    Map<String, Object> mapOfEmpresa = (Map<String, Object>) objByCnpj.getBody();
                    Map<String, Object> mapOfEstabelecimento = (Map<String, Object>) mapOfEmpresa.get("estabelecimento");
                    Map<String, Object> mapOfCidade = (Map<String, Object>) mapOfEstabelecimento.get("cidade");

                    empresa = new Empresa((String) mapOfEstabelecimento.get("cnpj"), (String) mapOfCidade.get("nome"), (String) mapOfEmpresa.get("razao_social"), (String) mapOfEstabelecimento.get("nome_fantasia"), (String) mapOfEstabelecimento.get("nome_fantasia"));
                }
            } catch (Throwable ignored) {
                return null;
            }
        }

        return empresa;
    }

    public ResponseEntity<?> save(Empresa empresa) {

        if (empresa.getCnpj() == null || empresa.getRazaoSocial() == null) {
            return ResponseEntity.badRequest().body("Um ou mais campos obrigatórios não preenchidos: cnpj, razao_social");
        } else {
            Empresa empresaAux = empresaRepository.findByCnpj(empresa.getCnpj());
            if (empresaAux != null) {
                empresa.setId(empresaAux.getId());
                empresa.setDhRegistro(empresaAux.getDhRegistro());
                empresaRepository.save(empresa);
                return ResponseEntity.ok("Registro atualizado com sucesso");
            }
        }

        if (empresa.getId() == null) {
            empresa.setDhRegistro(LocalDateTime.now());
        }
        empresaRepository.save(empresa);

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }
}
