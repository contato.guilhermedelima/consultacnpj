package br.com.guilhermedelimarocha.controllers;

import br.com.guilhermedelimarocha.models.Empresa;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;

@Named
@SessionScoped
public class EmpresaBean implements Serializable {

    private String cnpj;
    private Empresa empresa;
    private String endereco;
    private String telefone;

    private Client client;
    private final String API_URL = "http://localhost:9000/api/v1";

    @PostConstruct
    public void init() {
        client = ClientBuilder.newClient();
    }

    public void pesquisar() {
        try {
            empresa = client.target(API_URL + "/findByCnpj/")
                    .path(cnpj)
                    .request(MediaType.APPLICATION_JSON)
                    .get(Empresa.class);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao buscar CNPJ", e.getMessage()));
        }
    }

    public void salvar() {
        if (empresa != null) {
            empresa.setEndereco(endereco);
            empresa.setTelefone(telefone);
            try {
                client.target(API_URL + "/save")
                        .request(MediaType.APPLICATION_JSON)
                        .put(Entity.entity(empresa, MediaType.APPLICATION_JSON));
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Dados salvos com sucesso", null));
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar dados", e.getMessage()));
            }
        }
    }

    // Getters e Setters

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
